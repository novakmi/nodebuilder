//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.test.nodebuilder

import groovy.util.logging.Slf4j
import org.bitbucket.novakmi.nodebuilder.BuilderException
import org.testng.Assert
import org.testng.annotations.Test

@Slf4j
class PluginTreeNodeBuilderTest {

        //test empty builder
        @Test(groups = ["basic"])
        public void emptyBuilderTest() {
                log.trace('==> emptyBuilderTest')

                def builder = new TestPluginTreeNodeBuilder()
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 0)

                Assert.assertNull(builder.counter)
                builder.process()
                Assert.assertNotNull(builder.counter)

                builder.counter.assertEmptyCounter()

                log.trace('<== emptyBuilderTest')
        }

        @Test(groups = ["basic"])
        public void registerPluginTest() {
                log.trace('==> registerPluginTest')

                def plugin = new TestPlugin()
                plugin.assertEmptyPlugin()
                Assert.assertNull(plugin.getMyBuilder()) // builder not assigned

                def builder = new TestPluginTreeNodeBuilder()
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 0)
                builder.registerPlugin(plugin)
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 1)
                Assert.assertEquals(plugin.getMyBuilder(), builder) // builder is assigned aftr registration

                plugin.assertEmptyPlugin()

                builder = new TestPluginTreeNodeBuilder([plugin]) // register in constructor
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 1)
                builder.registerPlugin(plugin) // add same plugin once more
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 2)
                builder.unregisterPlugin(plugin)
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 1)

                plugin.assertEmptyPlugin()

                try {
                        builder.unregisterPlugin(new TestPlugin()) // try unregister unknown plugin
                } catch (BuilderException expected) {
                        Assert.assertTrue(expected.getMessage().contains("Plugin not registered"))
                }

                builder.unregisterPlugin(plugin)
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 0)

                plugin.assertEmptyPlugin()

                log.trace('<== registerPluginTest')
        }

        private _assertSimpleTreeInBuilderAndPlugins(TestPluginTreeNodeBuilder builder, plugins) {
                builder.assertSimpleTreeInBuilder()
                plugins.each { plugin ->
                        Assert.assertEquals(plugin.numberOfPluginReset, 0)
                        Assert.assertEquals(plugin.numberOfNodesProcessedBefore, builder.counter.numberOfNodesProcessed)
                        Assert.assertEquals(plugin.numberOfNodesProcessedAfter, builder.counter.numberOfNodesProcessed)
                }
        }

        private TestPluginTreeNodeBuilder _createAndProcessTestPluginTreeNodeBuilder(plugins) {
                def builder = new TestPluginTreeNodeBuilder(plugins)

                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), plugins.size())
                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)

                Assert.assertNull(builder.counter)
                builder.process()
                Assert.assertNotNull(builder.counter)

                return builder
        }

        private TestPluginTreeNodeBuilder _makePluginBuilderWithSimpleTree(plugins) {

                TestPluginTreeNodeBuilder builder = _createAndProcessTestPluginTreeNodeBuilder(plugins)
                _assertSimpleTreeInBuilderAndPlugins(builder, plugins)

                return builder
        }


        private _pluginsTest(plugins) {
                TestPluginTreeNodeBuilder builder = _makePluginBuilderWithSimpleTree(plugins)

                (1..3).each { // try several resets
                        builder.reset()
                        plugins.each { plugin ->
                                Assert.assertEquals(plugin.numberOfPluginReset, it)
                                Assert.assertEquals(plugin.numberOfNodesProcessedBefore, 0)
                                Assert.assertEquals(plugin.numberOfNodesProcessedAfter, 0)
                        }
                        Assert.assertNull(builder.counter)
                }
        }

        @Test(groups = ["basic"])
        public void pluginBuilderTest() {
                log.trace('==> pluginBuilderTest')

                def plugin = new TestPlugin()
                _pluginsTest([plugin])

                log.trace('<== pluginBuilderTest')
        }

        @Test(groups = ["basic"])
        public void pluginProcessedFullBuilderTest() {
                log.trace('==> pluginProcessedFullBuilderTest')

                def plugin = new TestPluginProcessedFull()
                TestPluginTreeNodeBuilder builder = _createAndProcessTestPluginTreeNodeBuilder([plugin])

                builder.counter.assertEmptyCounter()  // no processing  done in builder

                // processing done in plugin
                Assert.assertEquals(plugin.numberOfNodesProcessedBefore, 3)
                Assert.assertEquals(plugin.numberOfNodesProcessedAfter, 3)

                log.trace('<== pluginProcessedFullBuilderTest')
        }

        @Test(groups = ["basic"])
        public void morePluginBuilderTest() {
                log.trace('==> morePluginBuilderTest')

                def plugin1 = new TestPlugin()
                def plugin2 = new TestPlugin()
                def plugin3 = new TestPlugin()
                def plugins = [plugin1, plugin2, plugin3]
                _pluginsTest(plugins)

                log.trace('<== morePluginBuilderTest')
        }

        @Test(groups = ["basic"])
        public void removePluginBuilderTest() {
                log.trace('==> removePluginBuilderTest')

                def plugin1 = new TestPlugin()
                def plugin2 = new TestPlugin()
                def plugins = [plugin1, plugin2]

                TestPluginTreeNodeBuilder builder = _makePluginBuilderWithSimpleTree(plugins)

                builder.unregisterPlugin(plugin1)

                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), plugins.size() -1)
                builder.process() // this resets builder counter

                // plugin1 counts remains unchanged
                Assert.assertEquals(plugin1.numberOfNodesProcessedBefore, builder.counter.numberOfNodesProcessed)
                Assert.assertEquals(plugin1.numberOfNodesProcessedAfter, builder.counter.numberOfNodesProcessed)
                // plugin2 counts are influenced by second processing - multiply by 2
                Assert.assertEquals(plugin2.numberOfNodesProcessedBefore, builder.counter.numberOfNodesProcessed * 2)
                Assert.assertEquals(plugin2.numberOfNodesProcessedAfter, builder.counter.numberOfNodesProcessed * 2)

                builder.unregisterPlugin(plugin2)
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 0)
                builder.process() // this resets builder counter

                // plugin1 counts remains unchanged
                Assert.assertEquals(plugin1.numberOfNodesProcessedBefore, builder.counter.numberOfNodesProcessed)
                Assert.assertEquals(plugin1.numberOfNodesProcessedAfter, builder.counter.numberOfNodesProcessed)
                // plugin2 counts remains unchanged
                Assert.assertEquals(plugin2.numberOfNodesProcessedBefore, builder.counter.numberOfNodesProcessed * 2)
                Assert.assertEquals(plugin2.numberOfNodesProcessedAfter, builder.counter.numberOfNodesProcessed * 2)

                log.trace('<== removePluginBuilderTest')
        }

}
