//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.test.nodebuilder

import groovy.util.logging.Slf4j
import org.bitbucket.novakmi.nodebuilder.BuilderException
import org.testng.Assert
import org.testng.annotations.Test

@Slf4j
class TreeNodeBuilderTest {


        private def _testEmpty(TestTreeNodeBuilder builder) {

                Assert.assertNull(builder.counter)
                builder.process()
                Assert.assertNotNull(builder.counter)

                builder.counter.assertEmptyCounter()

                Assert.assertNull(builder.root)

                try {
                        builder.processEvenIfRootNull()
                } catch (BuilderException expected) {

                }
        }

        //test empty builder
        @Test(groups = ["basic"])
        public void emptyTreeNodeBuilderTest() {
                log.trace("==> emptyTreeNodeBuilderTest")

                _testEmpty(new TestTreeNodeBuilder())

                log.trace("<== emptyTreeNodeBuilderTest")
        }

        @Test(groups = ["basic"])
        public void emptyTreeNodeBuilderBeforeAfterTest() {
                log.trace("==> emptyTreeNodeBuilderBeforeAfterTest")

                _testEmpty(new TestBeforeAfterTreeNodeBuilder())

                log.trace("<== emptyTreeNodeBuilderBeforeAfterTest")
        }

        def _builderTest(builder) {
                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)

                // both process and  processEvenIfRootNull should yield same result
                [builder.&process, builder.&processEvenIfRootNull].each {process ->
                        process()
                        builder.assertSimpleTreeInBuilder()
                }
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderTest() {
                log.trace("==> simpeNodeBuilderTest")

                def builder = new TestTreeNodeBuilder()
                _builderTest(builder)
                Assert.assertEquals(builder.counter.numberOfNodesBeforeChildrenProcessed, 0)
                Assert.assertEquals(builder.counter.numberOfNodesAfterChildrenProcessed, 0)
                Assert.assertEquals(builder.getRootNode(),builder.getMyRootNode())

                log.trace("<== simpeNodeBuilderTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderBeforeAfterTest() {
                log.trace("==> simpeNodeBuilderBeforeAfterTest")

                def builder = new TestBeforeAfterTreeNodeBuilder()
                _builderTest(builder)
                Assert.assertEquals(builder.counter.numberOfNodesBeforeChildrenProcessed, builder.counter.numberOfNodesProcessed - 1)
                Assert.assertEquals(builder.counter.numberOfNodesAfterChildrenProcessed, builder.counter.numberOfNodesProcessed - 1)
                Assert.assertEquals(builder.getRootNode(),builder.getMyRootNode())

                log.trace("<== simpeNodeBuilderBeforeAfterTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderOneLevelTest() {
                log.trace("==> simpeNodeBuilderOneLevelTest")

                def builder = new TestTreeNodeBuilder()

                builder.node()

                builder.process()

                Assert.assertEquals(builder.counter.numberOfNodesProcessed, 1)
                Assert.assertEquals(builder.counter.maxDepth, 1)
                Assert.assertEquals(builder.counter.numberOfAllChildren, 0)
                Assert.assertEquals(builder.counter.maxChildrenPerNode, 0)
                Assert.assertEquals(builder.counter.numberOfAllAttributes, 0)
                Assert.assertEquals(builder.counter.maxAttributesPerNode, 0)
                Assert.assertEquals(builder.root.name, 'node')
                Assert.assertNull(builder.root.value)
                Assert.assertEquals(builder.counter.numberOfNodesBeforeChildrenProcessed, 0)
                Assert.assertEquals(builder.counter.numberOfNodesAfterChildrenProcessed, 0)
                Assert.assertEquals(builder.getRootNode(),builder.getMyRootNode())

                log.trace("<== simpeNodeBuilderOneLevelTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderTwoLevelTest() {
                log.trace("==> simpeNodeBuilderTwoLevelTest")

                def builder = new TestTreeNodeBuilder()

                builder.'root'() {
                        child()
                }

                builder.process()

                Assert.assertEquals(builder.counter.numberOfNodesProcessed, 2)
                Assert.assertEquals(builder.counter.maxDepth, 2)
                Assert.assertEquals(builder.counter.numberOfAllChildren, 1)
                Assert.assertEquals(builder.counter.maxChildrenPerNode, 1)
                Assert.assertEquals(builder.counter.numberOfAllAttributes, 0)
                Assert.assertEquals(builder.counter.maxAttributesPerNode, 0)
                Assert.assertEquals(builder.root.name, 'root')
                Assert.assertEquals(builder.root.children[0].name, 'child')
                Assert.assertEquals(builder.counter.numberOfNodesBeforeChildrenProcessed, 0)
                Assert.assertEquals(builder.counter.numberOfNodesAfterChildrenProcessed, 0)
                Assert.assertNull(builder.root.value)
                Assert.assertEquals(builder.root.children[0].value, null)


                log.trace("<== simpeNodeBuilderTwoLevelTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderFailTest() {
                log.trace("==> simpeNodeBuilderFailTest")

                def builder = new TestFailTreeNodeBuilder()
                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)

                try {
                        builder.process()
                } catch (BuilderException expected) {
                        Assert.assertTrue(expected.getMessage().contains("TestFailTreeNodeBuilder: processNode always fails"))
                }

                log.trace("<== simpeNodeBuilderFailTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderFailBeforeTest() {
                log.trace("==> simpeNodeBuilderFailBeforeTest")

                def builder = new TestFailBeforeTreeNodeBuilder()
                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)

                try {
                        builder.process()
                } catch (BuilderException expected) {
                        Assert.assertTrue(expected.getMessage().contains("TestFailBeforeTreeNodeBuilder: processNodeBeforeChildren always fails"))
                }

                log.trace("<== simpeNodeBuilderFailBeforeTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderFailAfterTest() {
                log.trace("==> simpeNodeBuilderFailAfterTest")

                def builder = new TestFailAfterTreeNodeBuilder()
                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)

                try {
                        builder.process()
                } catch (BuilderException expected) {
                        Assert.assertTrue(expected.getMessage().contains("TestFailAfterTreeNodeBuilder: processNodeAfterChildren always fails"))
                }

                log.trace("<== simpeNodeBuilderFailAfterTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderResetTest() {
                log.trace("==> simpeNodeBuilderResetTest")

                def builder = new TestTreeNodeBuilder()

                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)
                builder.process()
                builder.assertSimpleTreeInBuilder(builder)

                builder.reset()
                _testEmpty(builder)

                log.trace("<== simpeNodeBuilderResetTest")
        }

        @Test(groups = ["basic"])
        public void simpeNodeBuilderValueAttributeTest() {
                log.trace("==> simpeNodeBuilderValueAttributeTest")

                def builder = new TestTreeNodeBuilder()
                builder.node1('node1Val', at1: 'node1At1') {
                        node2('node2Val') {
                                node3('node3Val', at1: 'node2At1', at2: 'node2At2')
                        }
                        node4()
                }

                builder.process()

                Assert.assertEquals(builder.counter.numberOfNodesProcessed, 4)
                Assert.assertEquals(builder.counter.maxDepth, 3)
                Assert.assertEquals(builder.counter.numberOfAllChildren, 3)
                Assert.assertEquals(builder.counter.maxChildrenPerNode, 2)
                Assert.assertEquals(builder.counter.numberOfAllAttributes, 3)
                Assert.assertEquals(builder.counter.maxAttributesPerNode, 2)

                Assert.assertEquals(builder.root.name, 'node1')
                Assert.assertEquals(builder.root.value, 'node1Val')
                Assert.assertEquals(builder.root.attributes.size(), 1)
                Assert.assertEquals(builder.root.attributes.at1, 'node1At1')

                Assert.assertEquals(builder.root.children[0].name, 'node2')
                Assert.assertEquals(builder.root.children[0].value, 'node2Val')
                Assert.assertEquals(builder.root.children[0].attributes.size(), 0)

                Assert.assertEquals(builder.root.children[0].children[0].name, 'node3')
                Assert.assertEquals(builder.root.children[0].children[0].value, 'node3Val')
                Assert.assertEquals(builder.root.children[0].children[0].attributes.size(), 2)
                Assert.assertEquals(builder.root.children[0].children[0].attributes.at1, 'node2At1')
                Assert.assertEquals(builder.root.children[0].children[0].attributes.at2, 'node2At2')

                Assert.assertEquals(builder.root.children[1].name, 'node4')
                Assert.assertEquals(builder.root.children[1].value, null)
                Assert.assertEquals(builder.root.children[1].attributes.size(), 0)

                log.trace("<== simpeNodeBuilderValueAttributeTest")
        }

        @Test(groups = ["basic"])
        public void findNodeTest() {
                log.trace("==> findNodeTest")

                def builder = new TestTreeNodeBuilder()
                Assert.assertNull(builder.findNode('node1'))
                Assert.assertNull(builder.findNode('node2'))
                Assert.assertNull(builder.findNode('node3'))
                Assert.assertNull(builder.findNode('node4'))

                TestTreeNodeBuilder.fillBuilderWithSimpleTree(builder)
                Assert.assertNotNull(builder.findNode('node1'))
                Assert.assertNotNull(builder.findNode('node2'))
                Assert.assertNotNull(builder.findNode('node3'))
                Assert.assertNull(builder.findNode('node4'))  // node 4 not created

                def node = builder.findNode('node1')
                Assert.assertEquals(node.children.size(), 1)
                Assert.assertEquals(node.children[0].name, 'node2')

                node = builder.findNode('node2')
                Assert.assertEquals(node.children.size(), 1)
                Assert.assertEquals(node.children[0].name, 'node3')

                node = builder.findNode('node3')
                Assert.assertEquals(node.children.size(), 0)

                builder.reset()
                _testEmpty(builder)

                log.trace("<== findNodeTest")
        }

        @Test(groups = ["basic"])
        public void aliasTreeNodeBuilderTest() {
                log.trace("==> aliasTreeNodeBuilderTest")

                def builder = new TestTreeNodeBuilder()
                builder.declareAlias('import_', 'import')
                builder.declareAlias('try_', 'try')

                builder.node() {
                        import_('test_import')
                        try_('test_try')
                }

                builder.process()

                Assert.assertEquals(builder.root.name, 'node')
                Assert.assertEquals(builder.root.children[0].name, 'import')
                Assert.assertEquals(builder.root.children[0].value, 'test_import')
                Assert.assertEquals(builder.root.children[1].name, 'try')
                Assert.assertEquals(builder.root.children[1].value, 'test_try')

                //builder can convert alias itself
                Assert.assertEquals(builder.convertAlias("import_"), "import")
                Assert.assertEquals(builder.convertAlias("try_"), "try")

                log.trace("<== aliasTreeNodeBuilderTest")
        }

        @Test(groups = ["basic"])
        public void configMapTreeNodeBuilderTest() {
                log.trace("==> configMapTreeNodeBuilderTest")

                def builder = new TestTreeNodeBuilder([config1:true, config2:"val2"])
                Assert.assertEquals(builder.getConfigVal("config1"), true)
                Assert.assertEquals(builder.getConfigVal("config2"), "val2")
                Assert.assertEquals(builder.getConfigVal("config3"), null)

                builder.node1('node1Val', at1: 'node1At1') {
                        node2('node2Val', config3: true)
                        node3()
                }
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root, "config1"))
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root, "config2"))
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root, "at1"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root, "config3"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root, "at2"))

                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root.children[0], "config1"))
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root.children[0], "config2"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root.children[0], "at1"))
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root.children[0], "config3"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root.children[0], "at2"))

                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root.children[1], "config1"))
                Assert.assertTrue(builder.isKeyInConfigOrNodeAttr(builder.root.children[1], "config2"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root.children[1], "at1"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root.children[1], "config3"))
                Assert.assertFalse(builder.isKeyInConfigOrNodeAttr(builder.root.children[1], "at2"))

                log.trace("<== configMapTreeNodeBuilderTest")
        }

}
