//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.test.nodebuilder

import groovy.util.logging.Slf4j
import org.bitbucket.novakmi.nodebuilder.TextPluginTreeNodeBuilder
import org.testng.Assert
import org.testng.annotations.Test

@Slf4j
class TextPluginTreeNodeBuilderTest {

        //test empty builder
        @Test(groups = ["basic"])
        public void emptyTextBuilderTest() {
                log.trace('==> emptyTextBuilderTest')

                def builder = new TestTextPluginTreeNodeBuilder()
                Assert.assertEquals(builder.getText(), '')

                log.trace('<== emptyTextBuilderTest')
        }

        //test builder reset
        @Test(groups = ["basic"])
        public void resetTextBuilderTest() {
                log.trace('==> resetTextBuilderTest')

                def builder = new TestTextPluginTreeNodeBuilder()
                builder.hello()
                Assert.assertEquals(builder.getText(), 'hello\n')
                builder.resetText()
                Assert.assertEquals(builder.getText(), 'hello\n')
                builder.reset()
                Assert.assertEquals(builder.getText(), '')

                log.trace('<== resetTextBuilderTest')
        }

        private void _fillTextPluginTreeNodeBuilderWithTree(builder) {
                builder.hello('valHello', at1: 'at1Hello') {
                        helloChild(atChild1: 'atChild1Val')
                }
        }

        @Test(groups = ["basic"])
        public void treeTextBuilderTest() {
                log.trace('==> treeTextBuilderTest')

                def builder = new TestTextPluginTreeNodeBuilder() //default indent  2
                _fillTextPluginTreeNodeBuilderWithTree(builder)

                Assert.assertEquals(builder.getText(),
                    '''hello(valHello, at1:at1Hello)
  helloChild(atChild1:atChild1Val)
''')
                log.trace('<== treeTextBuilderTest')
        }

        @Test(groups = ["basic"])
        public void treeNodeTextBuilderTest() {
                log.trace('==> treeNodeTextBuilderTest')

                def builder = new TestTextPluginTreeNodeBuilder() //default indent  2
                _fillTextPluginTreeNodeBuilderWithTree(builder)

                Assert.assertEquals(builder.getNodeText(builder.getRootNode().children[0]),
                        '''helloChild(atChild1:atChild1Val)
''')
                log.trace('<== treeNodeTextBuilderTest')
        }

        @Test(groups = ["basic"])
        public void treeIndentTextBuilderTest() {
                log.trace('==> treeIndentTextBuilderTest')

                def builder = new TestTextPluginTreeNodeBuilder(4) // set indent to 4
                _fillTextPluginTreeNodeBuilderWithTree(builder)

                Assert.assertEquals(builder.getText(),
                    '''hello(valHello, at1:at1Hello)
    helloChild(atChild1:atChild1Val)
''')
                log.trace('<== treeIndentTextBuilderTest')
        }


        @Test(groups = ["basic"])
        public void textBuilderClosureTest() {
                log.trace('==> textBuilderClosureTest')

                def builder = new TestTextPluginTreeNodeBuilder(4) // set indent to 4

                def clsr = { helloClosure("closureVal", closureAttr: 'closureAttrVal') }

                builder.hello("helloVal", at1: 'at1Hello') {
                        helloChild(atChild: 'atChildVal')
                        //add closure
                        builder << clsr  //use builder
                        delegate << clsr //or delegate (recommended)
                }


                Assert.assertEquals(builder.getText(),
                        '''hello(helloVal, at1:at1Hello)
    helloChild(atChild:atChildVal)
    helloClosure(closureVal, closureAttr:closureAttrVal)
    helloClosure(closureVal, closureAttr:closureAttrVal)
''')
                builder.reset()
                builder << clsr
                Assert.assertEquals(builder.getText(),
                        '''helloClosure(closureVal, closureAttr:closureAttrVal)
''')

                builder.reset()

                // another way to add closure
                builder.invokeMethod('hello2', [clsr])
                Assert.assertEquals(builder.getText(),
                        '''hello2
    helloClosure(closureVal, closureAttr:closureAttrVal)
''')

                builder.reset()
                builder.hello {
                        // another way to add closure (under element)
                        delegate.invokeMethod('hello2', [clsr])
                }
                Assert.assertEquals(builder.getText(),
                        '''hello
    hello2
        helloClosure(closureVal, closureAttr:closureAttrVal)
''')

                // closures with parameters
                builder.reset()
                def clsr2 = { elem, val, attr-> "$elem"(val, closureAttr: attr) }
                builder.hello {
                        // closure with params has to be curried
                        delegate << clsr2.curry("helloClosureParam", "helloClosureParamVal", "helloClosureParamAttr")
                }
                Assert.assertEquals(builder.getText(),
                        '''hello
    helloClosureParam(helloClosureParamVal, closureAttr:helloClosureParamAttr)
''')

                // invoke method can be used as well
                builder.reset()
                builder.hello {
                        // closure with params has to be curried
                        delegate.invokeMethod("helloChild", [clsr2.curry("helloClosureParamInv",
                                "helloClosureParamInvVal", "helloClosureParamInvAttr")])
                }
                Assert.assertEquals(builder.getText(),
                        '''hello
    helloChild
        helloClosureParamInv(helloClosureParamInvVal, closureAttr:helloClosureParamInvAttr)
''')

                log.trace('<== textBuilderClosureTest')
        }

        @Test(groups = ["basic"])
        public void textBuilderMethodConflictTest() {
                log.trace('==> textBuilderMethodConflictTest')

                def builder = new TestTextPluginTreeNodeBuilder(4) // set indent to 4

                builder.hello('valHello', at1: 'at1Hello') {
                        delegate.invokeMethod('reset', [])  // use delegate (recommended)
                        builder.invokeMethod('reset', [[a:"a1"], // or builder
                                {
                                        node(b:"b1") {
                                                node(c:"c1")
                                                delegate.invokeMethod('leftShift', [d:"d1"])  // use delegate
                                                delegate(e:"e1") // delegate is ok
                                        }
                                }
                        ])
                }


                Assert.assertEquals(builder.getText(),
                        '''hello(valHello, at1:at1Hello)
    reset
    reset(a:a1)
        node(b:b1)
            node(c:c1)
            leftShift(d:d1)
            delegate(e:e1)
''')
                log.trace('<== textBuilderMethodConflictTest')
        }

        @Test(groups = ["basic"])
        public void textBuilderTrimTest() {
            log.trace('==> textBuilderTrimTest')

            def testLines = []
            def lines = TextPluginTreeNodeBuilder.trimAndQuoteLines(testLines, "\"")
            Assert.assertEquals(lines.size(), 0)
            ["First line", "  First line", "First line  ", "  First line  "].each {l ->
                lines = TextPluginTreeNodeBuilder.trimAndQuoteLines([l], "\"")
                Assert.assertEquals(lines.size(), 1)
                Assert.assertEquals(lines[0], "\"First line\"")
            }
            testLines = ["    First line", " Second line", " Third line"]

            ["","\"", "'"].each { q ->
                lines = TextPluginTreeNodeBuilder.trimAndQuoteLines(testLines, "$q")
                Assert.assertEquals(lines.size(), 3)
                Assert.assertEquals(lines[0], "${q}First line")
                Assert.assertEquals(lines[1], "${q == ""?q:" "}Second line")
                Assert.assertEquals(lines[2], "${q == ""?q:" "}Third line${q}")
            }
            
            log.trace('<== textBuilderTrimTest')
        }

}
