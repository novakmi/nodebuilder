//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.test.nodebuilder

import org.bitbucket.novakmi.nodebuilder.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.Assert

class BuilderCounter {
        def numberOfNodesProcessed = 0
        def numberOfNodesBeforeChildrenProcessed = 0
        def numberOfNodesAfterChildrenProcessed = 0
        def numberOfAllAttributes = 0
        def numberOfAllChildren = 0
        def maxAttributesPerNode = 0
        def maxChildrenPerNode = 0
        def maxDepth = 0

        public assertEmptyCounter() {
                logger.trace("==> assertEmptyCounter")

                Assert.assertEquals(numberOfNodesProcessed, 0)
                Assert.assertEquals(maxDepth, 0)
                Assert.assertEquals(numberOfAllChildren, 0)
                Assert.assertEquals(maxChildrenPerNode, 0)
                Assert.assertEquals(numberOfAllAttributes, 0)
                Assert.assertEquals(maxAttributesPerNode, 0)

                Assert.assertEquals(numberOfNodesBeforeChildrenProcessed, 0)
                Assert.assertEquals(numberOfNodesAfterChildrenProcessed, 0)

                logger.trace("<== assertEmptyCounter")
        }

        public assertSimpleTreeInCounter() {
                logger.trace("==> assertSimpleTreeInCounter")

                Assert.assertEquals(numberOfNodesProcessed, 3)
                Assert.assertEquals(maxDepth, 3)
                Assert.assertEquals(numberOfAllChildren, 2)
                Assert.assertEquals(maxChildrenPerNode, 1)
                Assert.assertEquals(numberOfAllAttributes, 0)
                Assert.assertEquals(maxAttributesPerNode, 0)

                logger.trace("<== assertSimpleTreeInCounter")
        }


        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(BuilderCounter.class);
}

class TestTreeNodeBuilder extends TreeNodeBuilder {

        // utility helper methods

        TestTreeNodeBuilder(configMap = null) {
                super(configMap)
        }

        static public void fillBuilderWithSimpleTree(builder) {
                logger.trace("==> fillBuilderWithSimpleTree")
                builder.node1 {
                        node2() {
                                node3()
                        }
                }
                logger.trace("<== fillBuilderWithSimpleTree")
        }

        public assertSimpleTreeInBuilder() {
                logger.trace("==> assertSimpleTreeInBuilder")

                counter.assertSimpleTreeInCounter()
                Assert.assertEquals(root.name, 'node1')
                Assert.assertNull(root.value)
                Assert.assertEquals(getRootNode(),getMyRootNode())

                logger.trace("<== assertSimpleTreeInBuilder")
        }

        // builder methods
        static public void _processNodeTreeBuilder(Logger logger, BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNode {}", node.name)

                BuilderCounter counter = opaque as BuilderCounter
                counter.numberOfNodesProcessed++
                counter.numberOfAllAttributes += node.attributes.size()
                counter.numberOfAllChildren += node.children.size()
                counter.maxAttributesPerNode = Math.max(node.attributes.size(), counter.maxAttributesPerNode)
                counter.maxChildrenPerNode = Math.max(node.children.size(), counter.maxChildrenPerNode)

                // calculate depth
                def depth = 1
                def nodeTmp = node
                while (nodeTmp.parent) {
                        depth++
                        nodeTmp = nodeTmp.parent
                }
                if (counter.maxDepth < depth) {
                        counter.maxDepth = depth
                }

                logger.trace("<== processNode")
        }

        public getMyRootNode() {
                logger.trace("==> getMyRootNode")
                logger.trace("<== getMyRootNode")
                return getRootNode()
        }


        public def BuilderCounter counter

        @Override
        protected void processNode(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNode {}", node.name)
                _processNodeTreeBuilder(logger, node, opaque)
                logger.trace("<== processNode")
        }

        void processEvenIfRootNull() throws BuilderException {
                logger.trace("==> process")
                counter = new BuilderCounter()

                processTree(root, counter)

                logger.trace("<== process")
        }

        void process() throws BuilderException {
                logger.trace("==> process")
                counter = new BuilderCounter()

                if (root) {
                        processTree(root, counter)
                }

                logger.trace("<== process")
        }


        public void reset() {
                super.reset()
                counter = null
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestTreeNodeBuilder.class)
}

class TestFailTreeNodeBuilder extends TestTreeNodeBuilder {

        @Override
        protected void processNode(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNode {}", node.name)

                throw new BuilderException("TestFailTreeNodeBuilder: processNode always fails")

                logger.trace("<== processNode")
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestFailTreeNodeBuilder.class);

}

class TestBeforeAfterTreeNodeBuilder extends TestTreeNodeBuilder {

        @Override
        protected void processNodeBeforeChildren(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNodeBeforeChildren {}", node.name)
                BuilderCounter counter = opaque as BuilderCounter
                counter.numberOfNodesBeforeChildrenProcessed++
                logger.trace("<== processNodeBeforeChildren")
        }

        @Override
        protected void processNodeAfterChildren(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNodeAfterChildren {}", node.name)
                BuilderCounter counter = opaque as BuilderCounter
                counter.numberOfNodesAfterChildrenProcessed++
                logger.trace("<== processNodeAfterChildren")
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestBeforeAfterTreeNodeBuilder.class)

}

class TestFailBeforeTreeNodeBuilder extends TestTreeNodeBuilder {

        @Override
        protected void processNodeBeforeChildren(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNodeBeforeChildren {}", node.name)

                throw new BuilderException("TestFailBeforeTreeNodeBuilder: processNodeBeforeChildren always fails")

                logger.trace("<== processNodeBeforeChildren")
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestFailBeforeTreeNodeBuilder.class)

}

class TestFailAfterTreeNodeBuilder extends TestTreeNodeBuilder {

        @Override
        protected void processNodeAfterChildren(BuilderNode node, Object opaque) throws BuilderException {
                logger.trace("==> processNodeBeforeChildren {}", node.name)

                throw new BuilderException("TestFailAfterTreeNodeBuilder: processNodeAfterChildren always fails")

                logger.trace("<== processNodeAfterChildren")
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestFailAfterTreeNodeBuilder.class)

}

class TestPlugin extends NodeBuilderPlugin {

        public def numberOfPluginReset = 0
        public def numberOfNodesProcessedBefore = 0
        public def numberOfNodesProcessedAfter = 0

        public assertEmptyPlugin() {
                logger.trace("==> assertEmptyPlugin")

                Assert.assertEquals(numberOfPluginReset, 0)
                Assert.assertEquals(numberOfNodesProcessedBefore, 0)
                Assert.assertEquals(numberOfNodesProcessedBefore, 0)

                logger.trace("<== assertEmptyPlugin")
        }

        protected PluginResult processNodeBefore(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                logger.trace("==> processNodeBefore {}", node.name)

                numberOfNodesProcessedBefore++

                logger.trace("<== processNodeBefore")
                return PluginResult.PROCESSED_CONTINUE

        }

        protected PluginResult processNodeAfter(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                logger.trace("==> processNodeAfter {}", node.name)

                numberOfNodesProcessedAfter++

                logger.trace("<== processNodeAfter")
                return PluginResult.PROCESSED_CONTINUE
        }

        public void reset() {
                numberOfNodesProcessedBefore = 0
                numberOfNodesProcessedAfter = 0
                numberOfPluginReset++
        }

        private static final Logger logger = LoggerFactory.getLogger(TestPlugin.class)
}

class TestPluginProcessedFull extends TestPlugin {

        protected PluginResult processNodeBefore(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                logger.trace("==> processNodeBefore {}", node.name)

                numberOfNodesProcessedBefore++

                logger.trace("<== processNodeBefore")
                return PluginResult.PROCESSED_FULL

        }

        protected PluginResult processNodeAfter(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                logger.trace("==> processNodeAfter {}", node.name)

                numberOfNodesProcessedAfter++

                logger.trace("<== processNodeAfter")
                return PluginResult.PROCESSED_FULL
        }

        private static final Logger logger = LoggerFactory.getLogger(TestPluginProcessedFull.class)
}

class TestPluginTreeNodeBuilder extends PluginTreeNodeBuilder {

        public def BuilderCounter counter

        public TestPluginTreeNodeBuilder(plugins = null) {
                super(plugins)
        }

        @Override
        protected void processNode(BuilderNode node, Object opaque) {
                TestTreeNodeBuilder._processNodeTreeBuilder(logger, node, opaque)
        }

        void process() throws BuilderException {
                logger.trace("==> process")
                counter = new BuilderCounter()

                if (root) {
                        processTree(root, counter)
                }

                logger.trace("<== process")
        }

        public void reset() {
                super.reset()
                counter = null
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestPluginTreeNodeBuilder.class)
}

class TestTextPluginTreeNodeBuilder extends TextPluginTreeNodeBuilder {

        public TestTextPluginTreeNodeBuilder(indent = 2, plugins = null) {
                super(indent, plugins)
        }

        @Override
        protected void processNode(BuilderNode node, Object opaque) throws BuilderException {
                opaque.printIndent()
                opaque.print("$node.name")
                if (node.value || node.attributes) {
                        opaque.print("(")
                        if (node.value) {
                                opaque.print("${node.value}")
                                if (node.attributes) {
                                        opaque.print(", ")
                                }
                        }
                        if (node.attributes) {
                                node.attributes.eachWithIndex {it, i ->
                                        opaque.print("${it.key}:${it.value}")
                                        if (i - 1 < node.attributes.size) {
                                                opaque.print(", ")
                                        }
                                }
                        }
                        opaque.println(")")
                } else {
                        opaque.println("")
                }
        }

        //Initialize logging
        private static final Logger logger = LoggerFactory.getLogger(TestTextPluginTreeNodeBuilder.class)
}
