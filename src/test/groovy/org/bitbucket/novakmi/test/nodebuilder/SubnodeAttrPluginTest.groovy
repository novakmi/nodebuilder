//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.test.nodebuilder

import groovy.util.logging.Slf4j
import org.bitbucket.novakmi.nodebuilder.BuilderException
import org.bitbucket.novakmi.nodebuilder.BuilderNode
import org.bitbucket.novakmi.nodebuilder.SubnodesAttrPlugin
import org.testng.Assert
import org.testng.annotations.Test

@Slf4j //Initialize logging
class SubnodeAttrPluginTest {

        //test empty builder
        @Test(groups = ["basic"])
        public void registerSubnodeAttrPluginTest() {
                log.trace('==> registerSubnodeAttrPluginTest')

                def builder = new TestPluginTreeNodeBuilder()
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 0)
                builder.registerPlugin(new SubnodesAttrPlugin())
                Assert.assertEquals(builder.getNumberOfRegisteredPlugins(), 1)

                log.trace('<== registerSubnodeAttrPluginTest')
        }

        //test subnodes
        @Test(groups = ["basic"])
        public void addNodeSubnodeAttrPluginTest() {
                log.trace('==> addNodeSubnodeAttrPluginTest')

                def builder = new TestPluginTreeNodeBuilder()
                builder.registerPlugin(new SubnodesAttrPlugin())

                builder.rootNode('rootNode value')
                builder.process()
                Assert.assertEquals(builder.root.children.size(), 0)

                builder.reset()
                builder.rootNode('rootNode value', subnodes: ['one', 'two', 'three'])
                builder.process()
                Assert.assertEquals(builder.root.children.size(), 3)
                Assert.assertEquals(builder.root.children[0].name, 'one')
                Assert.assertEquals(builder.root.children[0].value, null)
                Assert.assertEquals(builder.root.children[1].name, 'two')
                Assert.assertEquals(builder.root.children[1].value, null)
                Assert.assertEquals(builder.root.children[2].name, 'three')
                Assert.assertEquals(builder.root.children[2].value, null)

                builder.reset()
                builder.rootNode('rootNode value', subnodes: ['one', ['two', 89], new BuilderNode(name: 'three')])
                builder.process()
                Assert.assertEquals(builder.root.children.size(), 3)
                Assert.assertEquals(builder.root.children.size(), 3)
                Assert.assertEquals(builder.root.children[0].name, 'one')
                Assert.assertEquals(builder.root.children[0].value, null)
                Assert.assertEquals(builder.root.children[1].name, 'two')
                Assert.assertEquals(builder.root.children[1].value, 89)
                Assert.assertEquals(builder.root.children[2].name, 'three')
                Assert.assertEquals(builder.root.children[2].value, null)

                log.trace('<== addNodeSubnodeAttrPluginTest')
        }

        //test SubnodesAttrPlugin exceptions
        @Test(groups = ["basic"])
        public void exceptionSubnodeAttrPluginTest() {
                log.trace('==> exceptionSubnodeAttrPluginTest')

                def builder = new TestPluginTreeNodeBuilder()
                builder.registerPlugin(new SubnodesAttrPlugin())

                // subnode has to be List
                builder.rootNode('rootNode value', subnodes: 'one')
                try {
                        builder.process()
                        Assert.fail()
                } catch (BuilderException expected) {
                        // do nothing
                }

                // subnode List element has to be String, List or BuilderNode
                builder.reset()
                builder.rootNode('rootNode value', subnodes: [4])
                try {
                        builder.process()
                        Assert.fail()
                } catch (BuilderException expected) {
                        // do nothing
                }

                // subnode List element has to be of size 2
                builder.reset()
                builder.rootNode('rootNode value', subnodes: [['aaa']])
                try {
                        builder.process()
                        Assert.fail()
                } catch (BuilderException expected) {
                        // do nothing
                }
                builder.reset()
                builder.rootNode('rootNode value', subnodes: [['aaa', 'bbb', 'ccc']])
                try {
                        builder.process()
                        Assert.fail()
                } catch (BuilderException expected) {
                        // do nothing
                }

                // subnode List element has to have String as first element
                builder.reset()
                builder.rootNode('rootNode value', subnodes: [[1, 2]])
                try {
                        builder.process()
                        Assert.fail()
                } catch (BuilderException expected) {
                        // do nothing
                }

                log.trace('<== exceptionSubnodeAttrPluginTest')
        }
}
