//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.nodebuilder

import groovy.transform.CompileStatic

/**
 * Enumeration to be returned as result from plugin processing
 */
enum PluginResult {
        NOT_ACCEPTED, // node not accepted by the plugin (the plugin does not recognize this node)
        PROCESSED_CONTINUE, // node processed by the plugin, do processing by processNode as well
        PROCESSED_FULL, // node processed by the plugin, do not  processing by processNode
}

abstract class NodeBuilderPlugin {

        private PluginTreeNodeBuilder myBuilder = null;

        /**
         * Process given node by the plugin before node (and its children) is processed by the builder
         * Override this method in Plugin implementation, if needed.
         * @param node to process by the plugin
         * @param opaque object to be passed during processing of nodes
         * @param pluginMap plugin Map  (passed between all plugin processing - plugin can use it to store information,
         *                      to communicate with other plugins)
         * @return PluginResult
         * @throws BuilderException on failure
         * @see PluginResult , BuilderNode
         */
        protected PluginResult processNodeBefore(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                return PluginResult.NOT_ACCEPTED
        }

        /**
         * Process given node by the plugin after node (and its children) is processed by the builder
         * Override this method in Plugin implementation, if needed.
         * @param node to process by the plugin
         * @param opaque object to be passed during processing of nodes
         * @param pluginMap plugin Map  (passed between to all plugin processing - plugin can use it to store information,
         *                      to communicate with other plugins)
         * @return PluginResult
         * @throws BuilderException on failure
         * @see PluginResult , BuilderNode
         */
        protected PluginResult processNodeAfter(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                return PluginResult.NOT_ACCEPTED
        }

        /**
         * Reset plugin.
         * Needed for plugins keeping state (processing) data.
         * Plugins are reset together with Builder
         */
        public void reset() {
                // do nothing as default
        }

        /**
         * Set last registered builder being used by this plugin.
         * This should not be usually called by program, but by builder itself when registering the plugin.
         */
        public setMyBuilder(PluginTreeNodeBuilder myBuilder) {
                this.myBuilder = myBuilder
        }

        /**
         *
         * @return Builder of this plugin or null if not set (= registered)
         */
        public PluginTreeNodeBuilder getMyBuilder() {
                return this.myBuilder
        }

}

abstract class PluginTreeNodeBuilder extends TreeNodeBuilder {

        private List<NodeBuilderPlugin> plugins = []

        /**
         * Create new PluginTreeNodeBuilder
         * @param plugins list of plugins to be added (no plugins by default)
         */
        public PluginTreeNodeBuilder(plugins = null, configMap = null) {
                super(configMap)
                plugins?.each {
                        registerPlugin(it)
                }
        }

        /**
         * Register plugin with the builder.
         * @param plugin
         */
        public void registerPlugin(final NodeBuilderPlugin plugin) {
                plugins += plugin
                plugin.setMyBuilder(this)
        }

        /**
         * Remove plugin from builder
         * @param plugin reference to plugin to remove
         * @throws BuilderException plugin not registered
         */
        public void unregisterPlugin(plugin) throws BuilderException {
                if (!plugins.contains(plugin)) {
                        throw new BuilderException("Plugin not registered with the builder!")
                }
                plugins.remove(plugin)
        }

        public int getNumberOfRegisteredPlugins() {
                return plugins.size()
        }

        @Override protected void processTree(BuilderNode rootNode, Object nodeOpaque, Object treeOpaque = null)
        throws BuilderException {
                Map pluginMap = treeOpaque as Map

                if (!pluginMap) {
                        pluginMap = [:]
                }

                // process all plugins  processNodeBefore
                boolean processedFull = false;
                for (l in plugins) {
                        PluginResult res = l.processNodeBefore(rootNode, nodeOpaque, pluginMap)
                        if (res == PluginResult.PROCESSED_FULL) {
                                processedFull = true // at least one plugin indicates processed full
                        }
                }

                if (!processedFull) {
                        processNode(rootNode, nodeOpaque)
                }
                if (rootNode.children.size()) {
                        processNodeBeforeChildren(rootNode, nodeOpaque)
                        for (it in rootNode.children) {
                                processTree(it, nodeOpaque, pluginMap)
                        }
                        processNodeAfterChildren(rootNode, nodeOpaque)
                }
                // process all plugins  processNodeAfter
                for (l in plugins) {
                        l.processNodeAfter(rootNode, nodeOpaque, (Map) pluginMap)
                }
        }

        /**
         * Reset root element of the builder.
         */
        @Override
        @CompileStatic //https://issues.apache.org/jira/browse/GROOVY-11193
        public void reset() {
                plugins?.each { // reset plugins
                        it.reset()
                }
                super.reset()
        }
}
