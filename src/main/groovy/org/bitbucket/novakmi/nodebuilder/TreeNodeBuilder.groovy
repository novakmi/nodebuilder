//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.nodebuilder

/**
 * Builder BuilderNode
 */
class BuilderNode {
        def name = ''   // name of the node (e.g myNode('text, at:'attribute') - name is 'myNode')
        def parent = null // parent node (or null if this is root node)
        def attributes = [:] //attributes map (e.g. myNode('text, at:'attribute') - attributes.at == 'attribute')
        def value = null //value of node (e.g. myNode('text, at:'attribute') - value is text)
        def children = [] //children nodes

        /**
         * Utility function to create dot separated full path of the node
         * @param node leaf node
         * @return String representing path to leaf node
         */
        static public String getNodePath(BuilderNode node) {
                String retVal = node.name
                while (node.parent) {
                        node = node.parent
                        retVal = "${node.name}.${retVal}"
                }
                return retVal
        }

        /**
         * Return shallow copy of this node.
         * @param node
         * @return shallow copy
         */

        public BuilderNode shallowCopy() {
                return new BuilderNode(name: this.name, value: this.value, attributes: this.attributes, parent: this.parent)
        }
}

class BuilderException extends Exception {
        public BuilderException(String reason) {
                super(reason)
        }
}

/**
 * @see Based on http://groovy.codehaus.org/Make+a+builder
 */
abstract class TreeNodeBuilder extends BuilderSupport {

        protected BuilderNode root = null //root node of the model
        private Map aliasMap = [:]
        protected Map configMap = null

        /**
         *
         * @param configMap
         */
        TreeNodeBuilder(configMap = null) {
                this.configMap = configMap
        }

        /**
         * Add to config map
         * @param configMapToAdd
         */
        public def addConfig(Map configMapToAdd) {
                if (this.configMap) {
                        this.configMap += configMapToAdd
                } else {
                        this.configMap = configMapToAdd
                }
        }

        /**
         * Return value in config map for given key
         * @param key
         * @return null or found value
         */
        public getConfigVal(key) {
                return configMap ? configMap[key] : null
        }


        /**
         * Find out if key has true value  in config or is true as node attribute
         * @param node
         * @param key
         * @return  true or false
         */
        public isKeyInConfigOrNodeAttr(node, key) {
                return  getConfigVal(key) || node?.attributes[key]
        }

        /**
         * Convert node name, if it is alias
         * @param nodeName
         * @return converted name or original node name, if alias not found
         */
        public String convertAlias(String nodeName) {
                def retVal = aliasMap[nodeName]
                if (!retVal) {
                        retVal = nodeName
                }
                return retVal
        }

        /**
         * Get list of aliases for the given node name
         * @param nodeName
         * @return list of aliases or null if no alias is found
         */
        public List getAliases(String nodeName) {
                def retVal = null
                def aliases = []
                aliasMap.findAll{ it.value == nodeName }.each{aliases << it?.key} // find all keys for value
                if (aliases.size()) {
                        retVal = aliases
                }
                return retVal
        }

        @Override protected void setParent(Object parent, Object child) {
                ((BuilderNode) parent).children.add(child)
                ((BuilderNode) child).parent = parent
                if (!root) root = parent as BuilderNode
        }

        @Override protected Object createNode(Object name) {
                def BuilderNode node = new BuilderNode(name: convertAlias(name))
                if (!root) {
                        root = node
                }
                return node
        }

        @Override protected Object createNode(Object name, Object value) {
                def BuilderNode node = new BuilderNode(name: convertAlias(name), value: value)
                if (!root) {
                        root = node
                }
                return node
        }

        @Override protected Object createNode(Object name, Map attributes) {
                def BuilderNode node = new BuilderNode(name: convertAlias(name), attributes: attributes)
                if (!root) {
                        root = node
                }
                return node
        }

        @Override protected Object createNode(Object name, Map attributes, Object value) {
                def BuilderNode node = new BuilderNode(name: convertAlias(name), value: value, attributes: attributes)
                if (!root) {
                        root = node
                }
                return node
        }

        /**
         * This function allows to declare alias for the node name.
         * If alias is used as a node name, it is automatically converted to the real name
         * @param aliasName
         * @param realName
         */
        public def declareAlias(String aliasName, String realName) {
              aliasMap[aliasName] =  realName
        }

        /**
         * Process node.
         * This method has to be overridden in derived builder implementation.
         * @param node to be processed
         * @param opaque opaque object to be passed for node processing
         * @throws BuilderException throw this exception if node is not accepted
         *              by builder (exception is used to indicate line) or on failure
         */
        abstract protected void processNode(BuilderNode node, Object opaque) throws BuilderException;

        /**
         * Process node before children nodes are processed.
         * Method is called after 'processNode'  and only if node has children nodes. It can be used to
         * indent output, add opening bracket, etc.
         * Default implementation does not do anything.
         * This method can be overridden in derived implementation.
         * @param node to be processed
         * @param opaque opaque object to be passed for node processing
         * @throws BuilderException on failure
         */
        protected void processNodeBeforeChildren(BuilderNode node, Object opaque) throws BuilderException {
                // do nothing
        }

        /**
         * Process node after children nodes are processed.
         * Method is called after 'processNode' and only if node has children nodes. It can be used to
         * indent output, add closing bracket, etc.
         * Default implementation does not do anything.
         * This method can be overridden in derived implementation.
         * @param node to be processed
         * @param opaque opaque object to be passed for node processing
         * @throws BuilderException on failure
         */
        protected void processNodeAfterChildren(BuilderNode node, Object opaque) throws BuilderException {
                // do nothing
        }

        /**
         * Process tree of nodes
         * This method can be overridden in derived implementation.
         * @param treeNode root node of the tree (can be also leaf), cannto be null!
         * @param node opaque object to be passed for node processing
         * @param tree opaque object to be passed for tree processing (recursion)
         * @throws BuilderException throw this exception if node is not accepted
         *              by builder (exception is used to indicate line) or is null or on failure
         */
        protected void processTree(BuilderNode treeNode, Object nodeOpaque = null, Object treeOpaque = null)
        throws BuilderException {
                if (!treeNode) {
                        throw new BuilderException("Cannot process 'null' as treeNode!")
                }
                processNode(treeNode, nodeOpaque)
                if (treeNode.children.size()) {
                        processNodeBeforeChildren(treeNode, nodeOpaque)
                        for (it in treeNode.children) {
                                processTree(it, nodeOpaque, treeOpaque)
                        }
                        processNodeAfterChildren(treeNode, nodeOpaque)
                }
        }

        /**
         * Reset root element of the builder.
         * This method does not reset configMap
         */
        public void reset() {
                root = null
        }

        /**
         * Find node of given name in subtree starting from specific node
         * Node children, grandchildren etc. must represent subtree, there cannot be cycle
         * @param node node where subtree starts
         * @param nodeName name to search for
         * @return uilderNode representing node with given name or null if node node found
         */
        protected BuilderNode findNodeFrom(BuilderNode node, String nodeName) {
                def retVal = null
                if (node)  {
                        if (node.name == nodeName) {
                                retVal = node
                        } else {
                                for (n in node.children) {
                                        retVal = findNodeFrom(n, nodeName)
                                        if (retVal != null) {
                                                break
                                        }
                                }
                        }
                }
                return retVal
        }
        
        /**
         * Find first node with given name.
         * The nodes in builder must represent tree, not a cycle!
         * @return BuilderNode representing node with given name or null if node node found
         */
        public BuilderNode findNode(String nodeName) {
                def retVal = findNodeFrom(root, nodeName)
                return retVal
        }

        /**
         * Get root node
         * @return reference to root node
         */
        public BuilderNode getRootNode() {
                return root
        }
}
