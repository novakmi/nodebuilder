//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)


package org.bitbucket.novakmi.nodebuilder

import groovy.transform.CompileStatic

abstract class TextPluginTreeNodeBuilder extends PluginTreeNodeBuilder {
        private IndentPrinter out
        private PrintWriter writer
        private StringWriter stringWriter
        private String builderTextBuffer = null

        /**
         * Create new TextPluginTreeNodeBuilder
         * @param indent number of spaces for indentation (default is 2)
         * @param plugins list of plugins to be added (no plugins by default)
         * @param configMap to be added (no configMap by default)
         */
        protected TextPluginTreeNodeBuilder(indent = 2, plugins = null, configMap = null) {
                super(plugins, configMap)
                stringWriter = new StringWriter()
                writer = new PrintWriter(stringWriter)
                out = new IndentPrinter(writer, " " * indent)
        }

        @Override
        protected void processNodeBeforeChildren(BuilderNode node, Object opaque) throws BuilderException {
                opaque.incrementIndent()
        }

        @Override
        protected void processNodeAfterChildren(BuilderNode node, Object opaque) throws BuilderException {
                opaque.decrementIndent()
        }

        /**
         * Function returning text built with builder for specific node
         * @param node node to build text for
         * @param params optional params map - reserved for future use
         * @return text built
         * @throws BuilderException
         */
        public String getNodeText(BuilderNode node, params = null) throws BuilderException {
                StringBuffer buffer = stringWriter.getBuffer()
                buffer.delete(0, buffer.length()) // clear buffer
                stringWriter.flush()
                if (node) {
                        processTree(node, out)
                }
                return buffer.toString()
        }

        /**
         * Function returning text built with builder.
         * This function is not part of builder public interface
         * If called more than once, returns previously cached text.
         * @param params optional params map - reserved for future use
         * @return text built
         * @throws BuilderException
         */
        protected String getTextBuffer(params = null) throws BuilderException {
                if (!builderTextBuffer) {  // reuse from previous run?
                        builderTextBuffer = getNodeText(root, params)
                }
                return builderTextBuffer
        }

        /**
         * Function returning text built by builder.
         * It can be overridden by subclass to modify the result.
         * If not overridden, it behaves in a same way like getTextBuffer.
         * @param params optional params map - reserved for child/future use
         * @return string with built text
         * @throws BuilderException
         */
        public String getText(params = null) throws BuilderException {
                return getTextBuffer(params)
        }

        /**
         * Reset text built in the builder, but keep root element
         * Use this method to rebuild text (e.g. with different set of plugins)
         */
        public void resetText() {
                builderTextBuffer = null
        }

        /**
         * Reset root element of the builder.
         * Use this method to start building yang from the beginning.
         */
        @Override
        @CompileStatic //https://issues.apache.org/jira/browse/GROOVY-11193
        public void reset() {
                resetText()
                out.setIndentLevel(0) // reset possible indentation
                super.reset()
        }

        /**
         * Helper function that writes built text to the file
         * @param filePath full file path
         */
        public writeToFile(String filePath) {
                new File(filePath).write(getText())
        }

        /**
         * Override leftShift operator to allow build from closure
         * (e.g. builder << cls)
         * @param cls closure to build from
         */
        public void leftShift(cls) {
                cls.delegate = this
                cls()
        }

        /**
         * Return quote string to be added around text, if needed
         * @param txt            the text
         * @param qouteStrings   the possible quotes strings (to check if quotes are already present
         * @param encloseStrings the strings that require quotes
         * @return empty string if quotes not required or first from qouteStrings ( qouteStrings[0[)
         */
        static public def getQuotes(txt, qouteStrings = ['"', "'"], encloseStrings = []) {
                def retVal = ''
                if (((txt instanceof String) || (txt instanceof GString)) && txt?.size() > 1) {
                        def enclosedWithQuotes = false
                        for (s in qouteStrings) {
                                if ((txt[0] == s) && txt[-1] == s) {
                                        enclosedWithQuotes = true
                                }
                        }
                        if (!enclosedWithQuotes) {
                                def encloseFound = false
                                for (s in encloseStrings) {
                                        if (txt.contains(s)) {
                                                encloseFound = true
                                        }
                                }
                                if (encloseFound) {
                                        retVal = qouteStrings[0]
                                }
                        }
                }
                return retVal
        }

        /**
         * Format (trim) several lines of strings and surround them with quotes, if needed.
         * @param lines   the lines (not trimmed)
         * @param quoteString (string representing quotes - usually " or ')
         * @return formatted lines, if quotes are used, first line starts with quotesString and last one ends with it
         *         (lines between are indented according to quote size)
         */
        static public def trimAndQuoteLines(lines, quoteString = "") {
                ArrayList newLines = []
                def quotesIndent = ""
                if (quoteString != "") {
                        quotesIndent = ' ' * quoteString.size()
                }
                lines.eachWithIndex { l, i ->
                        def line = ""
                        if (i == 0) {
                                line += quoteString
                        }
                        if (!l.trim().equals("")) {
                                if (i != 0) {
                                        line += quotesIndent
                                }
                                line += l.trim()
                        }
                        newLines.add(line)
                }
                if (newLines.size()) { // add quotes to the last line
                        newLines[-1] += quoteString
                }
                return newLines
        }
}
