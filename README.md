#Nodebuilder
Nodebuilder represents set of libraries supporting creation of various groovy builders.

Project is hosted at <https://bitbucket.org/novakmi/nodebuilder>

Implementation source code is in  `main/src` directory.  
Test source code is in  `main/tests` directory.  
Documentation files and source code is in  `main/doc` directory.  

Use [gradle][gradle_id] to build, test and package project.

Use [groovy][groovy_id] version 2.0.0 and later.

See `changelog.txt`.  
See `LICENSE`.

(c) Michal Novak (<it.novakmi@gmail.com>)

[gradle_id]: http://www.gradle.org/  "Gradle"
[groovy_id]: http://groovy.codehaus.org/  "Groovy"

## Notes

### Left shift operator of the `TextPluginTreeNodeBuilder`

Since version `1.0.0` this builder overrides the left shift operator `<<` to consume closures.

E.g.:

```groovy
def clsr = { 
    hello(at: 'HELLO')
}
```

One can build directly from closure:

```groovy
builder << clsr
```

or build from closure as child elements: 

```groovy
builder.node() {
    delegate << clsr // instead of builder one can use `delegate`
}
```

or build from closure with `invokeMethod` (ot using `<<`)

```groovy
builder.node() {
    delegate.invokeMethod("childNode", [clsr])
}
```

Closures with parameters has to be `curried` to pass params.

```groovy
def clsr = { elem, val, attr-> "$elem"(val, closureAttr: attr) }
builder.hello {        
    delegate << clsr.curry("helloClosureParam", "helloClosureParamVal", "helloClosureParamAttr")
}
```                

See also `TextPluginTreeNodeBuilderTest.textBuilderClosureTest`

### Handling conflicts with builder's native methods

One can use `invokeMethod` to build elements that conflict with builder's native methods (e.g. `reset`):

```groovy
builder.node() {    
    delegate.invokeMethod('reset', [[a:"a1"]]) // or use `builder` instead of `delegate`
}
```

See also `TextPluginTreeNodeBuilderTest.textBuilderMethodConflictTest`

### Usage with `groovy` Grapes

```groovy
//@GrabResolver(name = 'jcenterrepo', root = 'https://jcenter.bintray.com', m2Compatible = true) //needed only with older ver. of groovy
@Grab(group = 'org.bitbucket.novakmi', module = 'nodebuilder', version = '1.0.0')
```

### Usage with `gradle`

```groovy
dependencies {
        compile localGroovy()
        compile group: 'org.bitbucket.novakmi', name: 'nodebuilder', version: '1.0.0'
}
```
